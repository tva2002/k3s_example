resource "kubernetes_deployment" "blue" {
  metadata {
    name = "wallclockapi-blue"
    namespace = "terraform-test"
    labels = {
      app = "wallclockapi-blue"
      version ="v1.0"
    }
  }

  spec {
    replicas = 2

    selector {
      match_labels = {
        app = "wallclockapi-blue"
        version ="v1.0"
      }
    }

    template {
      metadata {
        labels = {
          app = "wallclockapi-blue"
          version ="v1.0"
        }
      }

      spec {
        container {
          image = "tva2002/wallclockapi:v1.0"
          name  = "wallclockapi-blue"
          port {
             container_port = 5000
               }

          resources {
            limits = {
              cpu    = "0.5"
              memory = "512Mi"
            }
            requests = {
              cpu    = "250m"
              memory = "50Mi"
            }
          }


          }
        }
      }
    }
  }
