resource "kubernetes_deployment" "green" {
  metadata {
    name = "wallclockapi-green"
    namespace = "terraform-test"
    labels = {
      app = "wallclockapi-green"
      version = "v1.1"
    }
  }

  spec {
    replicas = 2

    selector {
      match_labels = {
        app = "wallclockapi-green"
        version = "v1.1"
      }
    }

    template {
      metadata {
        labels = {
          app = "wallclockapi-green"
          version = "v1.1"
        }
      }

      spec {
        container {
          image = "tva2002/wallclockapi:v1.1"
          name  = "wallclockapi-green"
          port {
             container_port = 5000
               }

          resources {
            limits = {
              cpu    = "0.5"
              memory = "512Mi"
            }
            requests = {
              cpu    = "250m"
              memory = "50Mi"
            }
          }


          }
        }
      }
    }
  }
