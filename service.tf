resource "kubernetes_service" "wallclockapi-service" {
  metadata {
    name = "wallclockapi-service"
    namespace = "terraform-test"
    labels = {
      app ="wallclockapi-blue"
      version = "v1.0"
      }
  }
  spec {
    selector = {
      app = kubernetes_deployment.blue.metadata.0.labels.app
      version = "v1.0"
    }

    port {
#      node_port   = 30201
      port        = 5000
      target_port = 5000
      protocol = "TCP"
    }

#    type = "NodePort"
#     type = "LoadBalancer"
  }
}
