resource "kubernetes_ingress" "wallclockapi" {
  metadata {
    name = "wallclockapi"
    namespace = "terraform-test"
    annotations = {
      "nginx.ingress.kubernetes.io/ssl-redirect": false
      "nginx.ingress.kubernetes.io/from-to-www-redirect" = false
      "nginx.ingress.kubernetes.io/rewrite-target": "/$1"
      "kubernetes.io/ingress.class": "nginx"
    }
  }
  spec {
    rule {
      host = "my.local"
      http {
        path {
          path = "/(.*)"
          backend {
            service_name = kubernetes_service.wallclockapi-service.metadata.0.name
            service_port = 5000
          }
        }
      }
    }
  }
wait_for_load_balancer = true
}
